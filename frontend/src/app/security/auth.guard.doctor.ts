import { Injectable } from '@angular/core';
import {Router, CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot} from '@angular/router';
import { SecurityService } from './security.service';
import {MatSnackBar} from '@angular/material';

@Injectable()
export class AuthGuardDoctor implements CanActivate {
  constructor(public auth: SecurityService, public router: Router, public snackBar: MatSnackBar) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (!this.auth.isAuthenticatedDoctor()) {
      if (this.auth.isAuthenticatedPatient()) {
        this.openSnackBar('Twoje konto nie ma dostępu do tej strony', 'Zamknij');
        this.router.navigate(['home']);
      } else {
      this.router.navigate(['login'], {
        queryParams: {
          return: state.url
        }
      });
    }
      return false;
    }
    return true;
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000,
    });
  }
}
