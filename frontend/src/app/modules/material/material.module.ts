import { NgModule } from '@angular/core';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import {
  MatPaginatorModule,
  MatToolbarModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatTabsModule,
  MatSelectModule,
  MatAutocompleteModule,
  MatMenuModule,
  MatExpansionModule,
  MatStepperModule,
  MatSnackBarModule, MatTooltipModule, MatGridListModule, MatTreeModule, MatChipsModule
} from '@angular/material';
import { MatRadioModule } from '@angular/material/radio';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { CommonModule } from '@angular/common';
import { MatNativeDateModule } from '@angular/material';
import { MatDialogModule } from '@angular/material';

@NgModule({
  imports: [],
  exports: [
    MatAutocompleteModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatExpansionModule,
    MatChipsModule,
    MatStepperModule,
    MatSnackBarModule,
    MatTooltipModule,
    MatGridListModule,
    MatTableModule,
    MatPaginatorModule,
    MatRadioModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatMenuModule,
    MatButtonModule,
    MatTabsModule,
    MatSelectModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCheckboxModule,
    MatDatepickerModule,
    CommonModule,
    MatNativeDateModule,
    MatTreeModule,
    MatDialogModule,
  ],
  declarations: []
})
export class MaterialModule {}
