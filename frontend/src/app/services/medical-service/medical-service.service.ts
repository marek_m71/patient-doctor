import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {MedicalServiceDto} from '../../model/medical-service-dto';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';

const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class MedicalServiceService {

  constructor(private httpClient: HttpClient) {
  }

  findAll(): Observable<MedicalServiceDto[]> {
    return this.httpClient.get<MedicalServiceDto[]>(apiUrl + environment.medicalService.findAll);
  }
}
