import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {SpecializationDto} from '../../model/specialization-dto';
import {SpecializationAddForm} from '../../model/form/specialization-add-form';

const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class SpecializationService {

  constructor(private httpClient: HttpClient) {
  }

  findAll(): Observable<SpecializationDto[]> {
    return this.httpClient.get<SpecializationDto[]>(apiUrl + environment.specialization.specializations);
  }

  addToDoctor(newSpecializationForm: SpecializationAddForm): Observable<any> {
    return this.httpClient.post<any>(apiUrl + environment.specialization.specializations, JSON.stringify(newSpecializationForm));
  }

  deleteFromDoctor(idSpecialization: number, idDoctor: number) {
    return this.httpClient.delete(apiUrl + environment.specialization.specializations +
      idSpecialization + environment.specialization.doctor + idDoctor);
  }
}
