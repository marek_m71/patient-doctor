import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {OpinionForm} from '../../model/form/opinion-form';

const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class OpinionService {

  constructor(private httpClient: HttpClient) {
  }

  add(opinionForm: OpinionForm): Observable<any> {
    return this.httpClient.post<any>(apiUrl + environment.opinion.opinions, JSON.stringify(opinionForm));
  }
}
