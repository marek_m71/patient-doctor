import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {VisitDto} from '../../model/visit-dto';
import {VisitAddForm} from '../../model/form/visit-add-form';
import {OperationVisitForm} from '../../model/form/operation-visit-form';

const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class VisitService {

  constructor(private httpClient: HttpClient) {
  }

  find(id: number): Observable<VisitDto> {
    return this.httpClient.get<VisitDto>(apiUrl + environment.visit.visits + id);
  }

  add(visitAddForm: VisitAddForm): Observable<any> {
    return this.httpClient.post<any>(apiUrl + environment.visit.visits, JSON.stringify(visitAddForm));
  }

  cancelPatientVisit(operationVisitForm: OperationVisitForm): Observable<any> {
    return this.httpClient.post(apiUrl + environment.visit.patientCancel, JSON.stringify(operationVisitForm));
  }

  cancelDoctorVisit(operationVisitForm: OperationVisitForm): Observable<any> {
    return this.httpClient.post(apiUrl + environment.visit.doctorCancel, JSON.stringify(operationVisitForm));
  }

  deleteDoctorVisit(id: number): Observable<any> {
    return this.httpClient.delete(apiUrl + environment.visit.visits + id);
  }

}
