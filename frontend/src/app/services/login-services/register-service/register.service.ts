import { Injectable } from '@angular/core';
import {PatientRegisterForm} from '../../../model/form/patient-register-form';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {DoctorRegisterForm} from '../../../model/form/doctor-register-form';

const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private httpClient: HttpClient) { }

  addPatient(patient: PatientRegisterForm): Observable<any> {
    return this.httpClient.post(apiUrl + environment.patient.patients, JSON.stringify(patient));
  }

  addDoctor(doctor: DoctorRegisterForm): Observable<any> {
    return this.httpClient.post(apiUrl + environment.doctor.doctors, JSON.stringify(doctor));
  }
}
