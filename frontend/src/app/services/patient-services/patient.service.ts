import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {PatientDto} from '../../model/patient-dto';
import {MakeAppointmentForm} from '../../model/form/make-appointment-form';

const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class PatientService {

  constructor(private httpClient: HttpClient) {
  }

  find(id: number): Observable<PatientDto> {
    return this.httpClient.get<PatientDto>(apiUrl + environment.patient.patients + id);
  }

  update(id: number, patient: PatientDto): Observable<PatientDto> {
    return this.httpClient.put<PatientDto>(apiUrl + environment.patient.patients + id, JSON.stringify(patient));
  }

  makeAppointment(makeAppointmentForm: MakeAppointmentForm): Observable<any> {
    return this.httpClient.post(apiUrl + environment.patient.makeAppointment, JSON.stringify(makeAppointmentForm));
  }
}
