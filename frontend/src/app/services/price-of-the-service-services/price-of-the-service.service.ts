import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {PriceOfTheServiceAddForm} from '../../model/form/price-of-the-service-add-form';
const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class PriceOfTheServiceService {

  constructor(private httpClient: HttpClient) { }

  add(newPriceOfTheServiceForm: PriceOfTheServiceAddForm): Observable<any> {
    return this.httpClient.post<any>(apiUrl + environment.priceOfTheService.priceOfTheService, JSON.stringify(newPriceOfTheServiceForm));
  }

  delete(id: number): Observable<any> {
    return this.httpClient.delete<any>(apiUrl + environment.priceOfTheService.priceOfTheService + id);
  }
}
