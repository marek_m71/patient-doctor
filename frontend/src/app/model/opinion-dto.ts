export class OpinionDto {
  id: number;
  rate: number;
  opinion: string;
}
