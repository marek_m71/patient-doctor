import {VisitDetailsDoctorDto} from './visit-details-doctor-dto';

export class PatientDto {
  id: number;
  email: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  visitList: VisitDetailsDoctorDto[];
}
