import {DoctorInformationDto} from './doctor-information-dto';
import {OpinionDto} from './opinion-dto';

export class VisitDetailsDoctorDto {
  id: number;
  dateTimeVisit: Date;
  isAppointed: boolean;
  isCanceledByDoctor: boolean;
  doctor: DoctorInformationDto;
  opinion: OpinionDto;
}
