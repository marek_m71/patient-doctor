export class PriceOfTheServiceAddForm {
  idDoctor: number;
  name: string;
  price: number;
}
