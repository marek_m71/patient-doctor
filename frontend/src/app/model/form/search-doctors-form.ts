import {SpecializationDto} from '../specialization-dto';

export class SearchDoctorsForm {
  city: string;
  specialization: SpecializationDto;
}
