export class PatientInformationDto {
  id: number;
  email: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
}
