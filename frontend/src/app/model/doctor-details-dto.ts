import {SpecializationDto} from './specialization-dto';
import {PriceOfTheServiceDto} from './price-of-the-service-dto';
import {VisitDetailsPatientDto} from './visit-details-patient-dto';

export class DoctorDetailsDto {
  id: number;
  email: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  city: string;
  specializationList: SpecializationDto[];
  priceList: PriceOfTheServiceDto[];
  visitList: VisitDetailsPatientDto[];
}
