import {Component, OnInit} from '@angular/core';
import {PatientRegisterForm} from '../../../model/form/patient-register-form';
import {RegisterService} from '../../../services/login-services/register-service/register.service';
import {Router} from '@angular/router';
import {SpecializationDto} from '../../../model/specialization-dto';
import {DoctorService} from '../../../services/doctor-services/doctor.service';
import {DoctorRegisterForm} from '../../../model/form/doctor-register-form';
import {MedicalServiceDto} from '../../../model/medical-service-dto';
import {PriceOfTheServiceDto} from '../../../model/price-of-the-service-dto';
import {MatSnackBar} from '@angular/material';
import {VisitForm} from '../../../model/form/visit-form';
import {SpecializationService} from '../../../services/specialization-service/specialization.service';
import {MedicalServiceService} from '../../../services/medical-service/medical-service.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  patientDto: PatientRegisterForm = new PatientRegisterForm();
  doctorDto: DoctorRegisterForm = new DoctorRegisterForm();
  specializationList: SpecializationDto[] = [];
  medicalServiceList: MedicalServiceDto[] = [];
  visitAddList: VisitForm[] = [];
  priceOfTheServiceDto: PriceOfTheServiceDto = new PriceOfTheServiceDto();
  visit = {date: '', time: []};
  listHours = [];
  errorPatient = false;
  errorDescriptionPatient = '';
  errorDoctor = false;
  errorDescriptionDoctor = '';
  minDate = new Date();

  constructor(private registerService: RegisterService,
              private router: Router,
              private doctorService: DoctorService,
              private snackBar: MatSnackBar,
              private specializationService: SpecializationService,
              private medicalServiceService: MedicalServiceService) {
  }

  ngOnInit() {
    this.generateHours();
    this.getSpecializations();
    this.getMedicalService();
  }

  addPatient() {
    this.registerService.addPatient(this.patientDto).subscribe(
      resp => {
        this.openSnackBar('Konto pacjenta zostało założone', 'Zamknij');
        this.router.navigate(['/login']);
      },
      error => {
        if (error['status'] === 409) {
          this.errorPatient = true;
          this.errorDescriptionPatient = 'Użytkownik o podanym adresie email istnieje';
        } else {
          if (error['status'] === 400) {
            this.errorPatient = true;
            this.errorDescriptionPatient = 'Nieznany błąd podczas rejestracji.';
          } else {
            this.errorPatient = true;
            this.errorDescriptionPatient = 'Nieznany błąd podczas rejestracji (serwer nieosiągalny).';
          }
        }
      }
    );
  }

  addDoctor() {
    console.log(this.doctorDto);
    this.registerService.addDoctor(this.doctorDto).subscribe(
      resp => {
        this.openSnackBar('Konto lekarza zostało założone', 'Zamknij');
        this.router.navigate(['/login']);
      },
      error => {
        console.log(error);
        if (error['status'] === 409) {
          this.errorDoctor = true;
          this.errorDescriptionDoctor = 'Użytkownik o podanym adresie email istnieje';
        } else {
          if (error['status'] === 400) {
            this.errorDoctor = true;
            this.errorDescriptionDoctor = 'Nieznany błąd podczas rejestracji.';
          } else {
            this.errorDoctor = true;
            this.errorDescriptionDoctor = 'Nieznany błąd podczas rejestracji (serwer nieosiągalny).';
          }
        }
      }
    );
  }

  getSpecializations() {
    this.specializationService.findAll().subscribe(
      resp => {
        this.specializationList = resp;
      },
      error => {
        this.openSnackBar('Ups, wystąpił problem z serwerem, spróbuj ponownie', 'Zamknij');
      }
    );
  }

  getMedicalService() {
    this.medicalServiceService.findAll().subscribe(
      resp => {
        this.medicalServiceList = resp;
      },
      error => {
        this.openSnackBar('Ups, wystąpił problem z serwerem, spróbuj ponownie', 'Zamknij');
      }
    );
  }

  addPriceOfTheService() {
    let exist = false;
    const x = new PriceOfTheServiceDto();
    x.price = this.priceOfTheServiceDto.price;
    x.name = this.priceOfTheServiceDto.name;
    this.doctorDto.priceList.forEach(r => {
      if (r.name === x.name) {
        exist = true;
      }
    });
    if (exist) {
      this.openSnackBar('Usługa o podanej nazwie już istnieje', 'Zamknij');
    } else {
      this.openSnackBar('Dodano usługę: ' + x.name + ' cena: ' + x.price, 'Zamknij');
      this.doctorDto.priceList.push(x);
    }
  }

  addVisit() {
    let exist = false;
    const tmp: VisitForm[] = [];
    this.visit.time.forEach(r => {
      const date = new Date(this.visit.date);
      date.setHours(r.toString().split(':')[0]);
      date.setMinutes(r.toString().split(':')[1]);
      const x = new VisitForm(date);
      tmp.push(x);
      this.doctorDto.visitList.forEach(s => {
        if (s.dateTimeVisit.toLocaleString() === x.dateTimeVisit.toLocaleString()) {
          exist = true;
        }
      });
    });
    if (exist) {
      this.openSnackBar('Jeden z wybranych terminów już istnieje', 'Zamknij');
    } else {
      this.openSnackBar('Poprawnie dodano wybrane terminy', 'Zamknij');
      tmp.forEach(r => this.doctorDto.visitList.push(r));
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 4000,
    });
  }

  resetDoctor() {
    this.errorDoctor = false;
    this.doctorDto.priceList = [];
    this.doctorDto.visitList = [];
  }

  resetPatient() {
    this.errorPatient = false;
  }

  generateHours() {
    const hour = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10',
      '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23'];
    const minute = ['00', '15', '30', '45'];
    hour.forEach(r => {
      minute.forEach(s => this.listHours.push(r + ':' + s));
    });
  }
}
