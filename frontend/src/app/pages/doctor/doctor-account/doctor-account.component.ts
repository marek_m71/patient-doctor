import {Component, OnInit} from '@angular/core';
import {SecurityService} from '../../../security/security.service';
import {MatSnackBar} from '@angular/material';
import {DoctorService} from '../../../services/doctor-services/doctor.service';
import {DoctorDto} from '../../../model/doctor-dto';
import {SpecializationService} from '../../../services/specialization-service/specialization.service';
import {MedicalServiceService} from '../../../services/medical-service/medical-service.service';
import {SpecializationDto} from '../../../model/specialization-dto';
import {MedicalServiceDto} from '../../../model/medical-service-dto';
import {VisitForm} from '../../../model/form/visit-form';
import {PriceOfTheServiceAddForm} from '../../../model/form/price-of-the-service-add-form';
import {SpecializationAddForm} from '../../../model/form/specialization-add-form';
import {VisitAddForm} from '../../../model/form/visit-add-form';
import {VisitService} from '../../../services/visit-services/visit.service';
import {PriceOfTheServiceService} from '../../../services/price-of-the-service-services/price-of-the-service.service';
import {PasswordService} from '../../../services/login-services/password-service/password.service';
import {ChangePasswordForm} from '../../../model/form/change-password-form';
import {Router} from '@angular/router';

@Component({
  selector: 'app-doctor-account',
  templateUrl: './doctor-account.component.html',
  styleUrls: ['./doctor-account.component.css']
})
export class DoctorAccountComponent implements OnInit {
  password = {oldPassword: '', password: '', confirmPassword: ''};
  specializationList: SpecializationDto[] = [];
  medicalServiceList: MedicalServiceDto[] = [];
  newPriceOfTheServiceForm: PriceOfTheServiceAddForm = new PriceOfTheServiceAddForm();
  newSpecializationForm: SpecializationAddForm = new SpecializationAddForm();
  starList: boolean[] = [true, true, true, true, true];

  hide = true;
  hide1 = true;
  hide2 = true;
  idDoctor: number;
  doctorDto: DoctorDto;
  visit = {date: '', time: []};
  listHours = [];
  filter = {date: new Date()};
  minDate = new Date();

  constructor(private securityService: SecurityService,
              private doctorService: DoctorService,
              private snackBar: MatSnackBar,
              private specializationService: SpecializationService,
              private medicalServiceService: MedicalServiceService,
              private visitService: VisitService,
              private priceOfTheServiceService: PriceOfTheServiceService,
              private passwordService: PasswordService,
              private router: Router) {
  }

  ngOnInit() {
    this.generateHours();
    this.idDoctor = this.securityService.getId();
    this.findDoctor(this.idDoctor);
  }

  findDoctor(id: number) {
    this.doctorService.find(id).subscribe(
      resp => {
        this.doctorDto = resp;
        this.doctorDto.visitList.forEach(r => {
          const date = new Date(r.dateTimeVisit);
          const userTimezoneOffset = date.getTimezoneOffset() * 60000;
          r.dateTimeVisit = new Date(date.getTime() - userTimezoneOffset);
        });
        this.getMedicalService();
        this.getSpecializations();
      },
      error => {
        this.openSnackBar('Ups, wystąpił problem z serwerem, spróbuj ponownie', 'Zamknij');
      }
    );
  }

  updateDoctor() {
    this.doctorService.update(this.doctorDto.id, this.doctorDto).subscribe(
      resp => {
        this.doctorDto = resp;
        this.doctorDto.visitList.forEach(r => {
          const date = new Date(r.dateTimeVisit);
          const userTimezoneOffset = date.getTimezoneOffset() * 60000;
          r.dateTimeVisit = new Date(date.getTime() - userTimezoneOffset);
        });
        this.getSpecializations();
        this.getMedicalService();
      },
      error => {
        this.openSnackBar('Ups, wystąpił problem z serwerem, spróbuj ponownie', 'Zamknij');
      }
    );
  }

  removePriceOfTheService(idPriceOfTheService: number) {
    this.priceOfTheServiceService.delete(idPriceOfTheService).subscribe(
      resp => {
        this.updateDoctor();
      },
      error => {
        this.openSnackBar('Ups, wystąpił problem z serwerem, spróbuj ponownie', 'Zamknij');
      }
    );
  }

  removeSpecialization(idSpecialization: number) {
    this.specializationService.deleteFromDoctor(idSpecialization, this.idDoctor).subscribe(
      resp => {
        this.updateDoctor();
      },
      error => {
        this.openSnackBar('Ups, wystąpił problem z serwerem, spróbuj ponownie', 'Zamknij');
      }
    );
  }

  addSpecialization() {
    this.newSpecializationForm.idDoctor = this.idDoctor;
    this.specializationService.addToDoctor(this.newSpecializationForm).subscribe(
      resp => {
        this.updateDoctor();
        this.newSpecializationForm = new SpecializationAddForm();
      },
      error => {
        this.openSnackBar('Ups, wystąpił problem z serwerem, spróbuj ponownie', 'Zamknij');
      }
    );
  }

  addPriceOfTheService() {
    let exist = false;
    const x = new PriceOfTheServiceAddForm();
    x.idDoctor = this.idDoctor;
    x.price = this.newPriceOfTheServiceForm.price;
    x.name = this.newPriceOfTheServiceForm.name;
    this.doctorDto.priceList.forEach(r => {
      if (r.name === x.name) {
        exist = true;
      }
    });
    if (exist) {
      this.openSnackBar('Usługa o podanej nazwie już istnieje', 'Zamknij');
    } else {
      this.openSnackBar('Dodano usługę: ' + x.name + ' cena: ' + x.price, 'Zamknij');

      this.priceOfTheServiceService.add(x).subscribe(
        resp => {
          this.updateDoctor();
        },
        error => {
          this.openSnackBar('Ups, wystąpił problem z serwerem, spróbuj ponownie', 'Zamknij');
        }
      );
    }
  }

  addVisit() {
    let exist = false;
    const tmp: VisitForm[] = [];
    this.visit.time.forEach(r => {
      const date = new Date(this.visit.date);
      date.setHours(r.toString().split(':')[0]);
      date.setMinutes(r.toString().split(':')[1]);
      const x = new VisitForm(date);
      tmp.push(x);
      this.doctorDto.visitList.forEach(s => {
        if (s.dateTimeVisit.toLocaleString() === x.dateTimeVisit.toLocaleString()) {
          exist = true;
        }
      });
    });
    if (exist) {
      this.openSnackBar('Jeden z wybranych terminów już istnieje', 'Zamknij');
    } else {
      this.openSnackBar('Poprawnie dodano wybrane terminy', 'Zamknij');
      const newVisitForm: VisitAddForm = new VisitAddForm();
      newVisitForm.idDoctor = this.idDoctor;
      newVisitForm.visitList = tmp;

      this.visitService.add(newVisitForm).subscribe(
        resp => {
          this.updateDoctor();
        },
        error => {
          this.openSnackBar('Ups, wystąpił problem z serwerem, spróbuj ponownie', 'Zamknij');
        }
      );
    }
  }


  getSpecializations() {
    this.specializationService.findAll().subscribe(
      resp => {
        this.specializationList = [];
        resp.forEach(r => {
          let exist = false;
          this.doctorDto.specializationList.forEach(z => {
            if (z.id === r.id) {
              exist = true;
            }
          });
          if (!exist) {
            this.specializationList.push(r);
          }
        });
      },
      error => {
        this.openSnackBar('Ups, wystąpił problem z serwerem, spróbuj ponownie', 'Zamknij');
      }
    );
  }

  getMedicalService() {
    this.medicalServiceService.findAll().subscribe(
      resp => {
        this.medicalServiceList = resp;
      },
      error => {
        this.openSnackBar('Ups, wystąpił problem z serwerem, spróbuj ponownie', 'Zamknij');
      }
    );
  }


  changePassword() {
    if (this.matchPassword()) {
      const changePasswordForm = new ChangePasswordForm(this.idDoctor, this.password.oldPassword, this.password.password);
      this.passwordService.changePassword(changePasswordForm).subscribe(
        resp => {
          this.router.navigate(['doctor/visit-list']);
          this.openSnackBar('Hasło zostało zmienione', 'Zamknij');
        },
        error => {
          if (error['status'] === 409) {
            this.openSnackBar('Stare hasło jest niepoprawne', 'Zamknij');
          } else {
            this.openSnackBar('Ups, wystąpił problem z serwerem, spróbuj ponownie', 'Zamknij');
          }
        }
      );
    } else {
      this.openSnackBar('Podane hasła są różne', 'Zamknij');
    }
  }

  matchPassword(): boolean {
    return this.password.password === this.password.confirmPassword;
  }

  generateHours() {
    const hour = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10',
      '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23'];
    const minute = ['00', '15', '30', '45'];
    hour.forEach(r => {
      minute.forEach(s => this.listHours.push(r + ':' + s));
    });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 4000,
    });
  }
}
