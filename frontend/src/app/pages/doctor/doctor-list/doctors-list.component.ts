import {Component, OnInit} from '@angular/core';
import {DoctorDto} from '../../../model/doctor-dto';
import {DoctorService} from '../../../services/doctor-services/doctor.service';
import {Router} from '@angular/router';
import {MatDialog, MatDialogConfig, MatSnackBar} from '@angular/material';
import {SecurityService} from '../../../security/security.service';
import {OpinionAddDialogComponent} from '../../opinion/opinion-add-dialog/opinion-add-dialog.component';
import {OpinionListDialogComponent} from '../../opinion/opinion-list-dialog/opinion-list-dialog.component';

@Component({
  selector: 'app-doctors-list',
  templateUrl: './doctors-list.component.html',
  styleUrls: ['./doctors-list.component.css']
})
export class DoctorsListComponent implements OnInit {

  doctorList: DoctorDto[] = [];
  isEmpty: boolean;
  minDate = new Date();
  filter = {date: new Date()};
  nowDate = new Date();

  constructor(private doctorService: DoctorService,
              private router: Router,
              private snackBar: MatSnackBar,
              private securityService: SecurityService,
              private opinionListDialog: MatDialog) {
  }

  ngOnInit() {
    this.getDoctorList();
  }

  getDoctorList() {
    this.doctorList = [];
    if (this.doctorService.doctorList.length > 0) {
      this.isEmpty = false;
      this.doctorService.doctorList.forEach(r => {
        r.visitList.forEach(s => {
          const date = new Date(s.dateTimeVisit);
          const userTimezoneOffset = date.getTimezoneOffset() * 60000;
          s.dateTimeVisit = new Date(date.getTime() - userTimezoneOffset);
        });
        this.doctorList.push(r);
      });
    } else {
      this.isEmpty = true;
    }
  }

  openOpinion(doctor: DoctorDto) {
    const config = new MatDialogConfig();
    config.data = doctor.opinionList;
    config.disableClose = true;
    const dialogGrant = this.opinionListDialog.open(OpinionListDialogComponent, config);
    dialogGrant.afterClosed().subscribe(result => {
    });
  }

  makeAppoinment(idDoctor: number, idVisit: number) {
    if (!this.securityService.isAuthenticated()) {
      this.openSnackBar('Musisz się zalogować, aby umówić się na wizytę', 'Zamknij');
    }
    this.router.navigate(['/make-appointment/doctor/' + idDoctor + '/visit/' + idVisit]);
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000,
    });
  }
}
