import {Component, OnInit} from '@angular/core';
import {SecurityService} from '../../../security/security.service';
import {PatientService} from '../../../services/patient-services/patient.service';
import {PatientDto} from '../../../model/patient-dto';
import {MatDialog, MatDialogConfig, MatSnackBar} from '@angular/material';
import {VisitService} from '../../../services/visit-services/visit.service';
import {OpinionService} from '../../../services/opinion-services/opinion.service';
import {OpinionAddDialogComponent} from '../../opinion/opinion-add-dialog/opinion-add-dialog.component';
import {OperationVisitForm} from '../../../model/form/operation-visit-form';

@Component({
  selector: 'app-patient-visit-list',
  templateUrl: './patient-visit-list.component.html',
  styleUrls: ['./patient-visit-list.component.css']
})
export class PatientVisitListComponent implements OnInit {
  idPatient: number;
  patientDto: PatientDto;
  nowDate = new Date();
  starList: boolean[] = [true, true, true, true, true];

  constructor(private securityService: SecurityService,
              private patientService: PatientService,
              private snackBar: MatSnackBar,
              private visitService: VisitService,
              public opinionAddDialog: MatDialog) {
  }

  ngOnInit() {
    this.idPatient = this.securityService.getId();
    this.findPatient(this.idPatient);
  }

  findPatient(id: number) {
    this.patientService.find(id).subscribe(
      resp => {
        this.patientDto = resp;
        this.patientDto.visitList.forEach(r => {
          const date = new Date(r.dateTimeVisit);
          const userTimezoneOffset = date.getTimezoneOffset() * 60000;
          r.dateTimeVisit = new Date(date.getTime() - userTimezoneOffset);
        });
      },
      error => {
        this.openSnackBar('Ups, wystąpił problem z serwerem, spróbuj ponownie', 'Zamknij');
      }
    );
  }

  addOpinion(id: number) {
    const config = new MatDialogConfig();
    config.data = [id];
    config.disableClose = true;
    const dialogGrant = this.opinionAddDialog.open(OpinionAddDialogComponent, config);
    dialogGrant.afterClosed().subscribe(result => {
      this.findPatient(this.idPatient);
    });
  }

  cancelPatientVisit(id: number) {
    const operationVisitForm = new OperationVisitForm(id);
    this.visitService.cancelPatientVisit(operationVisitForm).subscribe(
      resp => {
        this.findPatient(this.idPatient);
      },
      error => {
        this.openSnackBar('Ups, wystąpił problem z serwerem, spróbuj ponownie', 'Zamknij');
      }
    );
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 4000,
    });
  }
}
