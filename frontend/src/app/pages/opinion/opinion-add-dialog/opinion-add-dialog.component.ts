import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {OpinionService} from '../../../services/opinion-services/opinion.service';
import {OpinionForm} from '../../../model/form/opinion-form';

@Component({
  selector: 'app-opinion-add-dialog',
  templateUrl: './opinion-add-dialog.component.html',
  styleUrls: ['./opinion-add-dialog.component.css']
})
export class OpinionAddDialogComponent implements OnInit {
  idVisit: number;
  opinionForm = new OpinionForm();
  starList: boolean[] = [true, true, true, true, true];
  rating: number;

  constructor(
    public dialogRef: MatDialogRef<OpinionAddDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private opinionService: OpinionService,
    private snackBar: MatSnackBar
  ) {
  }

  ngOnInit() {
    this.idVisit = this.data[0];
    this.opinionForm.idVisit = this.idVisit;
  }

  addOpinion() {
    this.opinionForm.rate = this.rating;
    this.opinionService.add(this.opinionForm).subscribe(
      resp => {
        this.openSnackBar('Twoja opinia została zapisana', 'Zamknij');
        this.dialogRef.close();
      },
      error => {
        this.openSnackBar('Ups, wystąpił problem z serwerem, spróbuj ponownie', 'Zamknij');
        this.dialogRef.close();
      }
    );
  }

  setStar(count: number) {
    this.rating = count + 1;
    for (let i = 0; i <= 4; i++) {
      this.starList[i] = i > count;
    }
  }

  cancel(): void {
    this.dialogRef.close();
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 4000,
    });
  }

}
