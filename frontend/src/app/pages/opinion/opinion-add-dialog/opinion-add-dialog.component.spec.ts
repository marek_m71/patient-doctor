import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpinionAddDialogComponent } from './opinion-add-dialog.component';

describe('OpinionAddDialogComponent', () => {
  let component: OpinionAddDialogComponent;
  let fixture: ComponentFixture<OpinionAddDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpinionAddDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpinionAddDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
