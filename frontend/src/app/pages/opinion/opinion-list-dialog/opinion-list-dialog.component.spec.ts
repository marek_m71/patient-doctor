import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpinionListDialogComponent } from './opinion-list-dialog.component';

describe('OpinionListDialogComponent', () => {
  let component: OpinionListDialogComponent;
  let fixture: ComponentFixture<OpinionListDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpinionListDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpinionListDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
