import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {OpinionDto} from '../../../model/opinion-dto';

@Component({
  selector: 'app-opinion-list-dialog',
  templateUrl: './opinion-list-dialog.component.html',
  styleUrls: ['./opinion-list-dialog.component.css']
})
export class OpinionListDialogComponent implements OnInit {
  opinionList: OpinionDto[] = [];
  starList: boolean[] = [true, true, true, true, true];

  constructor(
    public dialogRef: MatDialogRef<OpinionListDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: OpinionDto[]) {
  }

  ngOnInit() {
    this.opinionList = this.data;
    console.log(this.opinionList);
  }

  close() {
    this.dialogRef.close();
  }

}
