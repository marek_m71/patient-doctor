// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'http://localhost:8080',
  login: '/login',
  patient: {
    patients: '/patients/',
    visit: '/visit/',
    makeAppointment: '/patients/make-appointment/',
  },
  doctor: {
    doctors: '/doctors/',
    findSome: '/doctors/find/',
    details: '/details/',
  },
  visit: {
    visits: '/visits/',
    patientCancel: '/visits/patient-cancel/',
    doctorCancel: '/visits/doctor-cancel/',
  },
  specialization: {
    specializations: '/specializations/',
    doctor: '/doctor/'
  },
  medicalService: {
    medicalService: '/medical-service/',
    findAll: '/medical-service/',
  },
  priceOfTheService: {
    priceOfTheService: '/price-of-the-service/',
  },
  password: {
    change: '/password/change/',
  },
  opinion: {
    opinions: '/opinions/',
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related errorPatient stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an errorPatient is thrown.
 */
// import 'zone.js/dist/zone-errorPatient';  // Included with Angular CLI.
