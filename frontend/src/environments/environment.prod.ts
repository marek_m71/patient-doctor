export const environment = {
  production: false,
  apiUrl: 'http://localhost:8080',
  login: '/login',
  patient: {
    patients: '/patients/',
    visit: '/visit/',
    makeAppointment: '/patients/make-appointment/',
  },
  doctor: {
    doctors: '/doctors/',
    findSome: '/doctors/find/',
    details: '/details/',
  },
  visit: {
    visits: '/visits/',
    patientCancel: '/patient-cancel/',
    doctorCancel: '/doctor-cancel/',
  },
  specialization: {
    specializations: '/specializations/',
    doctor: '/doctor/'
  },
  medicalService: {
    medicalService: '/medical-service/',
    findAll: '/medical-service/',
  },
  priceOfTheService: {
    priceOfTheService: '/price-of-the-service/',
  },
  password: {
    change: '/password/change/',
  },
  opinion: {
    opinions: '/opinions/',
  }
};
