package pl.i5b4s1.ai.model;

import lombok.*;

import javax.persistence.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Table
@Entity(name = "opinions")
public class Opinion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_opinion")
    private Integer id;
    @Column(name = "rate")
    private Integer rate;
    @Column(name = "opinion")
    private String opinion;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_visit")
    private Visit visit;

}
