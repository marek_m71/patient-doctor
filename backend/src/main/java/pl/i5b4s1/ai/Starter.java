package pl.i5b4s1.ai;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.i5b4s1.ai.dto.form.PatientRegisterForm;
import pl.i5b4s1.ai.enums.RoleEnum;
import pl.i5b4s1.ai.service.PatientService;
import pl.i5b4s1.ai.service.RoleService;
import pl.i5b4s1.ai.service.SpecializationService;

@Component
public class Starter implements CommandLineRunner {
    private final PatientService patientService;
    private final RoleService roleService;
    private final SpecializationService specializationService;

    public Starter(PatientService patientService, RoleService roleService, SpecializationService specializationService) {
        this.patientService = patientService;
        this.roleService = roleService;
        this.specializationService = specializationService;
    }

    @Override
    public void run(String... args) throws Exception {
        specializationService.findOrCreateIfNotExist("Kardiologia");
        specializationService.findOrCreateIfNotExist("Laryngologia");
        specializationService.findOrCreateIfNotExist("Neurologia");
        specializationService.findOrCreateIfNotExist("Ortopedia");
        specializationService.findOrCreateIfNotExist("Okulista");
        specializationService.findOrCreateIfNotExist("Chirurg");
        roleService.findOrCreateIfNotExist(RoleEnum.ROLE_DOCTOR);
        roleService.findOrCreateIfNotExist(RoleEnum.ROLE_PATIENT);
        patientService.addPatientConfiguration(PatientRegisterForm.builder()
                .email("marek.marszelewski@wp.pl")
                .firstName("Marek")
                .lastName("Marszelewski")
                .password("admin")
                .phoneNumber("600600600").build());
    }
}
