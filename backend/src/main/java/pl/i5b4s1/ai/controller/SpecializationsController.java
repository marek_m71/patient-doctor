package pl.i5b4s1.ai.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.i5b4s1.ai.dto.SpecializationDTO;
import pl.i5b4s1.ai.dto.form.SpecializationAddForm;
import pl.i5b4s1.ai.service.SpecializationService;

import java.util.List;

@RestController
@RequestMapping("/specializations")
public class SpecializationsController {

    private final SpecializationService specializationService;

    public SpecializationsController(SpecializationService specializationService) {
        this.specializationService = specializationService;
    }

    @GetMapping
    public ResponseEntity<List<SpecializationDTO>> findAll(){
        return new ResponseEntity<>(specializationService.findAll(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity addToDoctor(@RequestBody SpecializationAddForm specializationAddForm) {
        specializationService.addToDoctor(specializationAddForm);
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping("{idSpecialization}/doctor/{idDoctor}")
    public ResponseEntity deleteFromDoctor(@PathVariable("idDoctor") int idDoctor, @PathVariable("idSpecialization") int idSpecialization){
        specializationService.deleteFromDoctor(idDoctor, idSpecialization);
        return new ResponseEntity(HttpStatus.OK);
    }
}
