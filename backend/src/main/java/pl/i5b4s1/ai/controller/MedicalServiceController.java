package pl.i5b4s1.ai.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.i5b4s1.ai.dto.MedicalServiceDTO;
import pl.i5b4s1.ai.service.MedicalServiceService;

import java.util.List;

@RestController
@RequestMapping("/medical-service")
public class MedicalServiceController {

    private final MedicalServiceService medicalServiceService;

    public MedicalServiceController(MedicalServiceService medicalServiceService) {
        this.medicalServiceService = medicalServiceService;
    }

    @GetMapping
    public ResponseEntity<List<MedicalServiceDTO>> findAll(){
        return new ResponseEntity<>(medicalServiceService.findAll(), HttpStatus.OK);
    }
}
