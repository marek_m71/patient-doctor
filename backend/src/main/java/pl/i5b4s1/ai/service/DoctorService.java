package pl.i5b4s1.ai.service;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.i5b4s1.ai.dto.*;
import pl.i5b4s1.ai.dto.form.DoctorRegisterForm;
import pl.i5b4s1.ai.dto.form.PriceOfTheServiceForm;
import pl.i5b4s1.ai.dto.form.SearchDoctorsForm;
import pl.i5b4s1.ai.dto.form.VisitForm;
import pl.i5b4s1.ai.exception.DuplicatedUserException;
import pl.i5b4s1.ai.model.*;
import pl.i5b4s1.ai.repository.DoctorRepository;
import pl.i5b4s1.ai.repository.SpecializationRepository;
import pl.i5b4s1.ai.repository.UserRepository;
import pl.i5b4s1.ai.repository.VisitRepository;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DoctorService {
    private final DoctorRepository doctorRepository;
    private final UserRepository userRepository;
    private final SpecializationRepository specializationRepository;
    private final MedicalServiceService medicalServiceService;
    private final RoleService roleService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final VisitRepository visitRepository;

    public DoctorService(DoctorRepository doctorRepository, UserRepository userRepository, SpecializationRepository specializationRepository, MedicalServiceService medicalServiceService, RoleService roleService, BCryptPasswordEncoder bCryptPasswordEncoder, VisitRepository visitRepository) {
        this.doctorRepository = doctorRepository;
        this.userRepository = userRepository;
        this.specializationRepository = specializationRepository;
        this.medicalServiceService = medicalServiceService;
        this.roleService = roleService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.visitRepository = visitRepository;
    }

    public void add(DoctorRegisterForm doctorRegisterForm){
        if(isEmailDuplicated(doctorRegisterForm.getEmail())) throw new DuplicatedUserException(doctorRegisterForm.getEmail());
        Doctor doctor = convertToAddDoctor(doctorRegisterForm);
        doctorRepository.save(doctor);
    }

    public DoctorDTO update(int id, DoctorDTO doctorDTO) {
        Doctor doctor = convertToUpdateDoctor(id, doctorDTO);
        doctorRepository.save(doctor);
        return convertToDoctorDto(doctor);
    }

    public List<DoctorDTO> findSome(SearchDoctorsForm searchDoctorsForm) {
        List<Doctor> doctorList = doctorRepository.findAll()
                .stream().filter(r -> (r.getSpecializationList()
                        .contains(findOrCreateIfNotExist(searchDoctorsForm.getSpecialization().getName())))
                        && (r.getCity().toLowerCase().contains(searchDoctorsForm.getCity().toLowerCase())))
                .collect(Collectors.toList());
        return doctorList.stream().map(this::convertToDoctorDto).collect(Collectors.toList());
    }

    public DoctorDTO find(int id) {
        Doctor doctor = doctorRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        return convertToDoctorDto(doctor);
    }

    public DoctorDetailsDTO findDetails(int id) {
        Doctor doctor = doctorRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        return convertToDoctorDetailsDto(doctor);
    }

    private Doctor convertToUpdateDoctor(int id, DoctorDTO doctorDTO) {
        Doctor doctor = doctorRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        doctor.setFirstName(doctorDTO.getFirstName());
        doctor.setLastName(doctorDTO.getLastName());
        doctor.setPhoneNumber(doctorDTO.getPhoneNumber());
        doctor.setCity(doctorDTO.getCity());
        return doctor;
    }

    private DoctorDetailsDTO convertToDoctorDetailsDto(Doctor in) {
        return DoctorDetailsDTO.builder()
                .id(in.getId())
                .firstName(in.getFirstName())
                .lastName(in.getLastName())
                .city(in.getCity())
                .email(in.getUser().getUsername())
                .phoneNumber(in.getPhoneNumber())
                .specializationList(convertToListSpecializationDto(in.getSpecializationList()))
                .priceList(convertToPriceListDto(in.getPriceList()))
                .visitList(convertToVisitDetailsPatientListDto(in.getVisitList()))
                .build();
    }

    private List<VisitDetailsPatientDTO> convertToVisitDetailsPatientListDto(List<Visit> in) {
        List<VisitDetailsPatientDTO> out = new ArrayList<>();
        in.stream().map(this::convertToVisitDetailsPatientDto).forEach(out::add);
        return out;
    }

    private VisitDetailsPatientDTO convertToVisitDetailsPatientDto(Visit in) {
        return VisitDetailsPatientDTO.builder()
                .id(in.getId())
                .dateTimeVisit(in.getDateTime())
                .isAppointed(in.getIsAppointed())
                .isCanceledByDoctor(in.getIsCanceledByDoctor())
                .patient(in.getIsAppointed() ? convertToPatientInformationDto(in.getPatient()) : null)
                .build();
    }

    private PatientInformationDTO convertToPatientInformationDto(Patient in) {
        return PatientInformationDTO.builder()
                .id(in.getId())
                .email(in.getUser().getUsername())
                .firstName(in.getFirstName())
                .lastName(in.getLastName())
                .phoneNumber(in.getPhoneNumber())
                .build();
    }

    private DoctorDTO convertToDoctorDto(Doctor in){
        return DoctorDTO.builder()
                .id(in.getId())
                .firstName(in.getFirstName())
                .lastName(in.getLastName())
                .city(in.getCity())
                .email(in.getUser().getUsername())
                .phoneNumber(in.getPhoneNumber())
                .specializationList(convertToListSpecializationDto(in.getSpecializationList()))
                .priceList(convertToPriceListDto(in.getPriceList()))
                .visitList(convertToVisitListDto(in.getVisitList()))
                .opinionList(convertToOpinionListDto(in.getVisitList()))
                .build();
    }

    private List<OpinionDTO> convertToOpinionListDto(List<Visit> in) {
        return in.stream().filter(r -> r.getOpinion()!=null)
                .map(z -> convertToOpinionDto(z.getOpinion())).collect(Collectors.toList());
    }

    private OpinionDTO convertToOpinionDto(Opinion in) {
        return OpinionDTO.builder()
                .id(in.getId())
                .rate(in.getRate())
                .opinion(in.getOpinion())
                .build();
    }

    private List<VisitDTO> convertToVisitListDto(List<Visit> in) {
        List<VisitDTO> out = new ArrayList<>();
        in.stream().map(this::convertToVisitDto).forEach(out::add);
        return out;
    }

    private VisitDTO convertToVisitDto(Visit in) {
        return VisitDTO.builder()
                .id(in.getId())
                .dateTimeVisit(in.getDateTime())
                .isAppointed(in.getIsAppointed())
                .isCanceledByDoctor(in.getIsCanceledByDoctor())
                .build();
    }

    private List<PriceOfTheServiceDTO> convertToPriceListDto(List<PriceOfTheService> in) {
        List<PriceOfTheServiceDTO> out = new ArrayList<>();
        in.stream().map(this::convertToPriceOfTheServiceDto).forEach(out::add);
        return out;
    }

    private PriceOfTheServiceDTO convertToPriceOfTheServiceDto(PriceOfTheService in) {
        return PriceOfTheServiceDTO.builder()
                .id(in.getId())
                .name(in.getMedicalService().getName())
                .price(in.getPrice()).build();
    }

    private Doctor convertToAddDoctor(DoctorRegisterForm in){
        Doctor doctor = Doctor.builder()
                .firstName(in.getFirstName())
                .lastName(in.getLastName())
                .user(User.builder()
                        .username(in.getEmail())
                        .password(bCryptPasswordEncoder.encode(in.getPassword()))
                        .role(roleService.findRoleDoctor())
                        .build())
                .phoneNumber(in.getPhoneNumber())
                .city(in.getCity())
                .specializationList(convertToListSpecialization(in.getSpecializationList()))
                .priceList(new ArrayList<>())
                .visitList(new ArrayList<>())
                .build();
        doctor.setPriceList(convertToPriceList(doctor, in.getPriceList()));
        doctor.setVisitList(convertToVisitList(doctor, in.getVisitList()));
        return doctor;
    }

    private List<Visit> convertToVisitList(Doctor doctor, List<VisitForm> in) {
        return in.stream().map(r -> convertToVisit(doctor, r)).collect(Collectors.toList());
    }

    private Visit convertToVisit(Doctor doctor, VisitForm in) {
        return Visit.builder()
                .dateTime(in.getDateTimeVisit())
                .isAppointed(false)
                .isCanceledByDoctor(false)
                .doctor(doctor)
                .build();
    }

    private List<PriceOfTheService> convertToPriceList(Doctor doctor, List<PriceOfTheServiceForm> in) {
        return in.stream().map(r -> convertToPriceOfTheService(doctor, r)).collect(Collectors.toList());
    }

    private PriceOfTheService convertToPriceOfTheService(Doctor doctor, PriceOfTheServiceForm in) {
        return PriceOfTheService.builder()
                .price(in.getPrice())
                .medicalService(findMedicalService(in.getName()))
                .doctor(doctor)
                .build();
    }

    private List<SpecializationDTO> convertToListSpecializationDto(List<Specialization> in) {
        List<SpecializationDTO> out = new ArrayList<>();
        in.stream().map(r -> SpecializationDTO.builder().id(r.getId()).name(r.getName()).build()).forEach(out::add);
        return out;
    }

    private List<Specialization> convertToListSpecialization(List<SpecializationDTO> in) {
        List<Specialization> out = new ArrayList<>();
        in.stream().map(r-> findOrCreateIfNotExist(r.getName())).forEach(out::add);
        return out;
    }

    private MedicalService findMedicalService(String name) {
        return medicalServiceService.findOrCreateIfNotExists(name);
    }

    private Specialization findOrCreateIfNotExist(String name){
        return specializationRepository.findByName(name).orElseGet(() -> save(name));
    }

    private Specialization save(String name) {
        return specializationRepository.save(Specialization.builder()
                .name(name)
                .build());
    }

    private Boolean isEmailDuplicated(String username){
        return userRepository.findByUsername(username).isPresent();
    }
}
