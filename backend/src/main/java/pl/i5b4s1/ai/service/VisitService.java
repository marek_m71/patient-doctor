package pl.i5b4s1.ai.service;

import org.springframework.stereotype.Service;
import pl.i5b4s1.ai.dto.VisitDTO;
import pl.i5b4s1.ai.dto.form.OperationVisitForm;
import pl.i5b4s1.ai.dto.form.VisitAddForm;
import pl.i5b4s1.ai.dto.form.VisitForm;
import pl.i5b4s1.ai.model.Doctor;
import pl.i5b4s1.ai.model.Visit;
import pl.i5b4s1.ai.repository.DoctorRepository;
import pl.i5b4s1.ai.repository.VisitRepository;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class VisitService {
    private final VisitRepository visitRepository;
    private final DoctorRepository doctorRepository;
    private final MessageService messageService;

    public VisitService(VisitRepository visitRepository, DoctorRepository doctorRepository, MessageService messageService) {
        this.visitRepository = visitRepository;
        this.doctorRepository = doctorRepository;
        this.messageService = messageService;
    }

    public VisitDTO findVisit(int id) {
        Visit visit = visitRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        return convertToVisitDto(visit);
    }

    public void patientCancelVisit(OperationVisitForm operationVisitForm) {
        Visit visit = visitRepository.findById(operationVisitForm.getIdVisit()).orElseThrow(EntityNotFoundException::new);
        visit.setPatient(null);
        visit.setIsAppointed(false);
        visitRepository.save(visit);
    }

    public void doctorCancelVisit(OperationVisitForm operationVisitForm) {
        Visit visit = visitRepository.findById(operationVisitForm.getIdVisit()).orElseThrow(EntityNotFoundException::new);
        messageService.sendEmailCancelVisit(visit.getPatient().getUser().getUsername(),
                                            visit.getDateTime(),
                                            visit.getDoctor().getFirstName() + " " + visit.getDoctor().getLastName(),
                                            visit.getDoctor().getPhoneNumber());
        visit.setIsCanceledByDoctor(true);
        visitRepository.save(visit);
    }

    public void deleteVisit(int id) {
        Visit visit = visitRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        visitRepository.delete(visit);
    }

    private VisitDTO convertToVisitDto(Visit in) {
        return VisitDTO.builder()
                .id(in.getId())
                .isAppointed(in.getIsAppointed())
                .isCanceledByDoctor(in.getIsCanceledByDoctor())
                .dateTimeVisit(in.getDateTime())
                .build();
    }

    public void addVisit(VisitAddForm visitAddForm) {
        Doctor doctor = doctorRepository.findById(visitAddForm.getIdDoctor()).orElseThrow(EntityNotFoundException::new);
        List<Visit> visitList = convertToVisitList(doctor, visitAddForm.getVisitList());
        doctor.getVisitList().addAll(visitList);
        doctorRepository.save(doctor);
    }

    private List<Visit> convertToVisitList(Doctor doctor, List<VisitForm> in) {
        return in.stream().map(r -> convertToVisit(doctor, r)).collect(Collectors.toList());
    }

    private Visit convertToVisit(Doctor doctor, VisitForm in) {
        return Visit.builder()
                .dateTime(in.getDateTimeVisit())
                .isAppointed(false)
                .isCanceledByDoctor(false)
                .doctor(doctor)
                .build();
    }
}
