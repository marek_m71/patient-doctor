package pl.i5b4s1.ai.service;

import org.springframework.stereotype.Service;
import pl.i5b4s1.ai.dto.form.PriceOfTheServiceAddForm;
import pl.i5b4s1.ai.model.Doctor;
import pl.i5b4s1.ai.model.MedicalService;
import pl.i5b4s1.ai.model.PriceOfTheService;
import pl.i5b4s1.ai.repository.DoctorRepository;
import pl.i5b4s1.ai.repository.PriceOfTheServiceRepository;

import javax.persistence.EntityNotFoundException;


@Service
public class PriceOfTheServiceService {
    private final DoctorRepository doctorRepository;
    private final PriceOfTheServiceRepository priceOfTheServiceRepository;
    private final MedicalServiceService medicalServiceService;

    public PriceOfTheServiceService(DoctorRepository doctorRepository, PriceOfTheServiceRepository priceOfTheServiceRepository, MedicalServiceService medicalServiceService) {
        this.doctorRepository = doctorRepository;
        this.priceOfTheServiceRepository = priceOfTheServiceRepository;
        this.medicalServiceService = medicalServiceService;
    }

    public void add(PriceOfTheServiceAddForm priceOfTheServiceAddForm) {
        Doctor doctor = doctorRepository.findById(priceOfTheServiceAddForm.getIdDoctor()).orElseThrow(EntityNotFoundException::new);
        PriceOfTheService priceOfTheService = convertToPriceOfTheService(doctor, priceOfTheServiceAddForm);
        doctor.getPriceList().add(priceOfTheService);
        doctorRepository.save(doctor);
    }

    public void remove(int id) {
        PriceOfTheService priceOfTheService = priceOfTheServiceRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        priceOfTheServiceRepository.delete(priceOfTheService);
    }

    private PriceOfTheService convertToPriceOfTheService(Doctor doctor, PriceOfTheServiceAddForm in) {
        return PriceOfTheService.builder()
                .price(in.getPrice())
                .medicalService(findMedicalService(in.getName()))
                .doctor(doctor)
                .build();
    }

    private MedicalService findMedicalService(String name) {
        return medicalServiceService.findOrCreateIfNotExists(name);
    }
}
