package pl.i5b4s1.ai.service;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.i5b4s1.ai.dto.*;
import pl.i5b4s1.ai.dto.form.MakeAppointmentForm;
import pl.i5b4s1.ai.dto.form.PatientRegisterForm;
import pl.i5b4s1.ai.exception.DuplicatedUserException;
import pl.i5b4s1.ai.model.*;
import pl.i5b4s1.ai.repository.PatientRepository;
import pl.i5b4s1.ai.repository.UserRepository;
import pl.i5b4s1.ai.repository.VisitRepository;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@Service
public class PatientService {
    private final PatientRepository patientRepository;
    private final UserRepository userRepository;
    private final RoleService roleService;
    private final VisitRepository visitRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public PatientService(PatientRepository patientRepository, UserRepository userRepository, RoleService roleService, VisitRepository visitRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.patientRepository = patientRepository;
        this.userRepository = userRepository;
        this.roleService = roleService;
        this.visitRepository = visitRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public void addPatient(PatientRegisterForm patientRegisterForm) {
        if (isEmailDuplicated(patientRegisterForm.getEmail()))
            throw new DuplicatedUserException(patientRegisterForm.getEmail());
        Patient patient = convertToPatient(patientRegisterForm);
        patientRepository.save(patient);
    }

    public PatientDTO updatePatient(int id, PatientDTO patientDTO) {
        Patient patient = patientRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        patient.setFirstName(patientDTO.getFirstName());
        patient.setLastName(patientDTO.getLastName());
        patient.setPhoneNumber(patientDTO.getPhoneNumber());
        patientRepository.save(patient);
        return convertToPatientDTO(patient);
    }

    public PatientDTO findPatient(Integer id) {
        Patient patient = patientRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        return convertToPatientDTO(patient);
    }

    public void addPatientConfiguration(PatientRegisterForm patientRegisterForm) {
        if (!isEmailDuplicated(patientRegisterForm.getEmail())) {
            Patient patient = convertToPatient(patientRegisterForm);
            patientRepository.save(patient);
        }
    }

    private PatientDTO convertToPatientDTO(Patient patient) {
        return PatientDTO.builder()
                .id(patient.getId())
                .email(patient.getUser().getUsername())
                .firstName(patient.getFirstName())
                .lastName(patient.getLastName())
                .phoneNumber(patient.getPhoneNumber())
                .visitList(convertToVisitDetailsDoctorListDto(patient.getVisitList()))
                .build();
    }

    public void makeAppointment(MakeAppointmentForm makeAppointmentForm) {
        Patient patient = patientRepository.findById(makeAppointmentForm.getIdPatient()).orElseThrow(EntityNotFoundException::new);
        Visit visit = visitRepository.findById(makeAppointmentForm.getIdVisit()).orElseThrow(EntityNotFoundException::new);
        visit.setIsAppointed(true);
        patient.addVisit(visit);
        patientRepository.save(patient);
    }

    private List<VisitDetailsDoctorDTO> convertToVisitDetailsDoctorListDto(List<Visit> in) {
        List<VisitDetailsDoctorDTO> out = new ArrayList<>();
        in.stream().map(this::convertToVisitDetailsDoctorDto).forEach(out::add);
        return out;
    }

    private VisitDetailsDoctorDTO convertToVisitDetailsDoctorDto(Visit in) {
        return VisitDetailsDoctorDTO.builder()
                .dateTimeVisit(in.getDateTime())
                .isAppointed(in.getIsAppointed())
                .isCanceledByDoctor(in.getIsCanceledByDoctor())
                .id(in.getId())
                .doctor(convertToDoctorInformationDto(in.getDoctor()))
                .opinion(convertToOpinionDto(in.getOpinion()))
                .build();
    }

    private OpinionDTO convertToOpinionDto(Opinion in) {
        if (in == null) return null;
        return OpinionDTO.builder()
                .id(in.getId())
                .rate(in.getRate())
                .opinion(in.getOpinion())
                .build();
    }

    private DoctorInformationDTO convertToDoctorInformationDto(Doctor in) {
        return DoctorInformationDTO.builder()
                .id(in.getId())
                .city(in.getCity())
                .email(in.getUser().getUsername())
                .firstName(in.getFirstName())
                .lastName(in.getLastName())
                .phoneNumber(in.getPhoneNumber())
                .priceList(convertToPriceListDto(in.getPriceList()))
                .specializationList(convertToListSpecializationDto(in.getSpecializationList()))
                .build();
    }

    private List<SpecializationDTO> convertToListSpecializationDto(List<Specialization> in) {
        List<SpecializationDTO> out = new ArrayList<>();
        in.stream().map(r -> SpecializationDTO.builder().name(r.getName()).build()).forEach(out::add);
        return out;
    }

    private List<PriceOfTheServiceDTO> convertToPriceListDto(List<PriceOfTheService> in) {
        List<PriceOfTheServiceDTO> out = new ArrayList<>();
        in.stream().map(this::convertToPriceOfTheServiceDto).forEach(out::add);
        return out;
    }

    private PriceOfTheServiceDTO convertToPriceOfTheServiceDto(PriceOfTheService in) {
        return PriceOfTheServiceDTO.builder()
                .id(in.getId())
                .name(in.getMedicalService().getName())
                .price(in.getPrice()).build();
    }

    private Patient convertToPatient(PatientRegisterForm in) {
        return Patient.builder()
                .firstName(in.getFirstName())
                .lastName(in.getLastName())
                .user(User.builder()
                        .username(in.getEmail())
                        .password(bCryptPasswordEncoder.encode(in.getPassword()))
                        .role(roleService.findRolePatient())
                        .build())
                .phoneNumber(in.getPhoneNumber())
                .visitList(new ArrayList<>())
                .build();
    }

    private Boolean isEmailDuplicated(String username) {
        return userRepository.findByUsername(username).isPresent();
    }
}
