package pl.i5b4s1.ai.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.i5b4s1.ai.enums.RoleEnum;
import pl.i5b4s1.ai.model.Role;
import pl.i5b4s1.ai.repository.RoleRepository;

@Service
public class RoleService {
    private final RoleRepository roleRepository;

    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public Role findOrCreateIfNotExist(RoleEnum roleEnum){
        return roleRepository.findByRole(roleEnum.toString()).orElseGet(() -> save(roleEnum));
    }

    private Role save(RoleEnum roleEnum) {
        return roleRepository.save(Role.builder()
                .role(roleEnum.toString())
                .build());
    }

    Role findRoleDoctor(){
        return findOrCreateIfNotExist(RoleEnum.ROLE_DOCTOR);
    }

    Role findRolePatient(){
        return findOrCreateIfNotExist(RoleEnum.ROLE_PATIENT);
    }
}
