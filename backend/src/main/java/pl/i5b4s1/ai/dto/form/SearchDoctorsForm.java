package pl.i5b4s1.ai.dto.form;

import lombok.*;
import pl.i5b4s1.ai.dto.SpecializationDTO;

@Builder
@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class SearchDoctorsForm {
    private String city;
    private SpecializationDTO specialization;
}
