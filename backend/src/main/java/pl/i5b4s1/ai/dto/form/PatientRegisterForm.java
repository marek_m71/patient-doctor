package pl.i5b4s1.ai.dto.form;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
public class PatientRegisterForm {
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String phoneNumber;
}
