package pl.i5b4s1.ai.dto;

import lombok.*;

@Setter
@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class MedicalServiceDTO {
    private String name;
}
