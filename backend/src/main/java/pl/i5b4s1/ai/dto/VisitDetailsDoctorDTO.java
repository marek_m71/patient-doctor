package pl.i5b4s1.ai.dto;

import lombok.*;

import java.time.LocalDateTime;

@Setter
@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class VisitDetailsDoctorDTO {
    private Integer id;
    private LocalDateTime dateTimeVisit;
    private Boolean isAppointed;
    private Boolean isCanceledByDoctor;
    private DoctorInformationDTO doctor;
    private OpinionDTO opinion;
}
