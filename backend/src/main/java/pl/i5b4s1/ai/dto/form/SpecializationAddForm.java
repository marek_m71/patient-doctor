package pl.i5b4s1.ai.dto.form;

import lombok.*;
import pl.i5b4s1.ai.dto.SpecializationDTO;

@Setter
@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SpecializationAddForm {
    private Integer idDoctor;
    private SpecializationDTO specialization;
}
