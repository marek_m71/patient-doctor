package pl.i5b4s1.ai.dto.form;

import lombok.*;

import java.time.LocalDateTime;

@Setter
@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class VisitForm {
    private LocalDateTime dateTimeVisit;
}
