package pl.i5b4s1.ai.dto.form;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
public class PriceOfTheServiceForm {
    private String name;
    private Double price;
}
