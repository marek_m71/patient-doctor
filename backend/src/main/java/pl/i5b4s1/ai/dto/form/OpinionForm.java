package pl.i5b4s1.ai.dto.form;

import lombok.*;

@Setter
@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class OpinionForm {
    private Integer idVisit;
    private Integer rate;
    private String opinion;
}
