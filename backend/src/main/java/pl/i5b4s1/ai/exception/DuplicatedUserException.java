package pl.i5b4s1.ai.exception;

public class DuplicatedUserException extends RuntimeException {
    public DuplicatedUserException(String email) {
        super("Użytkownik o adresie email: " + email + " już istnieje");
    }
}
